package com.saurabh.weatherapp;

import com.saurabh.weatherapp.model.WeatherModel;
import java.util.List;

public interface WeatherContract {

    interface View {

        void setAdapterData(List<com.saurabh.weatherapp.model.List> weatherList);
    }

    interface Presenter {

        void doAPICall(double latitude, double longitude, int count, String auth);
    }
}

package com.saurabh.weatherapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.saurabh.weatherapp.model.List;
import com.saurabh.weatherapp.util.util;

import java.util.ArrayList;

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.ViewHolder> {
    private java.util.List<List> weatherList = new ArrayList<>();
    Context context;

    WeatherAdapter(Context context){
        this.context = context;
    }

    public void setData(java.util.List<List> weatherList) {
        this.weatherList = weatherList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public WeatherAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_weather_detail, parent, false);
        return new WeatherAdapter.ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        List list = weatherList.get(position);

        Spanned strArea = Html.fromHtml("<font color=\"#000000\"><b>" + "Area : " +"</b></font>" + list.getName());
        Spanned strTemp = Html.fromHtml("<font color=\"#000000\"><b>" + "Temperature : " +"</b></font>" + util.convertKelvinToCelsius(list.getMain().getTemp()) + " \u2103");
        Spanned strHumidity = Html.fromHtml("<font color=\"#000000\"><b>" + "Humidity : " +"</b></font>" + list.getMain().getHumidity() +" %");

        viewHolder.tvArea.setText(strArea);
        viewHolder.tvTemp.setText(strTemp);
        viewHolder.tvHumidity.setText(strHumidity);
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }



    class ViewHolder extends RecyclerView.ViewHolder  {
        TextView tvArea, tvTemp, tvHumidity;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvArea = itemView.findViewById(R.id.tv_area);
            tvTemp = itemView.findViewById(R.id.tv_temp);
            tvHumidity = itemView.findViewById(R.id.tv_humidity);

        }
    }
}

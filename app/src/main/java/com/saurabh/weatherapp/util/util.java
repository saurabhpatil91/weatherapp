package com.saurabh.weatherapp.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class util {
    public static double convertKelvinToCelsius(double temp){
        return (double) (temp - 273.15);
    }

    public static boolean amIConnected(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isInternetConnected(Context context){
        if(!amIConnected(context)){
            Toast.makeText(context, "Internet connection not available !", Toast.LENGTH_LONG).show();
        }
        return amIConnected(context);
    }

}

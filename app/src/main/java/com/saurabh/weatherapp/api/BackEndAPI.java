package com.saurabh.weatherapp.api;

import com.saurabh.weatherapp.model.WeatherModel;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface BackEndAPI {
    @Headers("Authorization:Bearer eb72938919e4b0895351147427866547")
    @GET("data/2.5/find?")
    Observable<Response<WeatherModel>> getWeatherList(@Query("lat") double latitude, @Query("lon") double longitude, @Query("cnt") int count, @Query("appid") String apiKey);
}

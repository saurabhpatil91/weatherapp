package com.saurabh.weatherapp.api;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BASE_URL = "http://api.openweathermap.org/";
    public static final String APIKey = "eb72938919e4b0895351147427866547";
    private static BackEndAPI backEndAPI = null;

    private ApiClient() {
    }

    public static BackEndAPI getBackEndAPI() {
        if (backEndAPI == null) {
            backEndAPI = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build().create(BackEndAPI.class);
        }
        return backEndAPI;
    }
}

package com.saurabh.weatherapp;

import android.util.Log;

import com.saurabh.weatherapp.api.ApiClient;
import com.saurabh.weatherapp.model.WeatherModel;

import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;

public class WeatherPresenter implements WeatherContract.Presenter {

    private WeatherContract.View view;

    public WeatherPresenter(WeatherContract.View view) {
        this.view = view;
    }

    @Override
    public void doAPICall(double latitude, double longitude, int count, String auth) {
        Observable<Response<WeatherModel>> observable = ApiClient.getBackEndAPI().getWeatherList(latitude,longitude,count, auth);
        if (observable != null) {
            observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<Response<WeatherModel>>() {
                        @Override
                        public void onSubscribe(Disposable d) {

                        }

                        @Override
                        public void onNext(Response<WeatherModel> headlineResponse) {
                            Log.e("URL",String.valueOf(headlineResponse.raw().request().url()));
                            view.setAdapterData(Objects.requireNonNull(headlineResponse.body()).getList());

                        }

                        @Override
                        public void onError(Throwable e) {
                            Log.e("ERROR",e.getMessage());
                        }

                        @Override
                        public void onComplete() {

                        }
                    });
        }
    }
}
